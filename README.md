# Declarative Imperative OpenWRT configs

[OpenWRT] is an embedded Linux distribution optimized for small routers and access points with minimal amounts of storage to work with.
[NixOS] is a general-purpose Linux distribution built from the ground up with declarative configuration in mind, usually using a bunch of storage to do its thing.
[dewclaw](./index.html) is what happens if you try to mush the two together even though you know very well that you shouldn't.

[OpenWRT]: https://openwrt.org/
[NixOS]: https://nixos.org/
